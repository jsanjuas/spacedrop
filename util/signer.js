const web3Utils = require('web3-utils');
const eutil = require('ethereumjs-util');

/* web3 v0.20.x and v1.x APIs differ, work around with these adapters */
function web0SignAdapterFactory(web3) {
  return async function(sender, h) {
    return web3.eth.sign(sender, h);
  };
}

function web1SignAdapterFactory(web3) {
  return async function(sender, h) {
    return web3.eth.sign(h, sender);
  };
}

async function sign(sender, h, web3signFn) {
  const signedData = await web3signFn(sender, h);

  const rsv = eutil.fromRpcSig(signedData);
  return {
    v: rsv.v,
    r: eutil.bufferToHex(rsv.r),
    s: eutil.bufferToHex(rsv.s),
  };
}

async function signAirdropERC20(token, sender, recipient, nTokens, nonce,
    web3signFn) {
  const h = web3Utils.soliditySha3(token, sender, recipient, nTokens, nonce);
  return await sign(sender, h, web3signFn);
}

async function signAirdropERC777(token, sender, recipient, nTokens, nonce,
    userData, web3signFn) {
  const h = web3Utils.soliditySha3(token, sender, recipient, nTokens, nonce,
    userData);
  return await sign(sender, h, web3signFn);
}

module.exports = {
  signAirdropERC20,
  signAirdropERC777,
  web0SignAdapterFactory,
  web1SignAdapterFactory,
};
