const Spacedrop = artifacts.require('./Spacedrop.sol');
const TestToken = artifacts.require('./TestToken.sol');
const {signAirdropERC20, web0SignAdapterFactory} = require('../util/signer.js');

/* truffle currently offers web3 0.20.x */
const signFn = web0SignAdapterFactory(web3);

function eqToString16(a, b) {
  assert.equal(a.toString(16), b.toString(16));
}

contract('Spacedrop', function(accounts) {
  beforeEach(async function() {
    this.sender = accounts[0];
    this.recipient = accounts[1];
    this.token = await TestToken.new();
    this.spacedrop = await Spacedrop.new(this.token.address);
  });

  it('1 approve/claim cycle for ERC20', async function() {
    const nTokens = 31337;
    const nonce = 0;

    const parms = await signAirdropERC20(this.token.address, this.sender,
      this.recipient, nTokens, nonce, signFn);

    const balanceSenderPre = await this.token.balanceOf(this.sender);
    const balanceRecipientPre = await this.token.balanceOf(this.recipient);

    await this.token.approve(this.spacedrop.address, nTokens);
    await this.spacedrop.claimTokensERC20(this.token.address, this.sender,
      this.recipient, nTokens, nonce, parms.v, parms.r, parms.s);

    const balanceSenderPost = await this.token.balanceOf(this.sender);
    const balanceRecipientPost = await this.token.balanceOf(this.recipient);

    eqToString16(balanceSenderPost, balanceSenderPre.minus(nTokens));
    eqToString16(balanceRecipientPost, balanceRecipientPre.plus(nTokens));
  });

  it('a claim consumes less than 100k gas', async function() {
    const parms = await signAirdropERC20(this.token.address, this.sender,
      this.recipient, 14, 0, signFn);

    await this.token.approve(this.spacedrop.address, 14);
    const r = await this.spacedrop.claimTokensERC20(this.token.address,
      this.sender, this.recipient, 14, 0, parms.v, parms.r, parms.s);

    assert.isBelow(r.receipt.gasUsed, 100000);
  });
});
