pragma solidity ^0.4.18;

import "../node_modules/zeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "../node_modules/zeppelin-solidity/contracts/token/ERC20/SafeERC20.sol";

interface ERC777i {
  function operatorSend(address from, address to, uint256 amount, bytes userData, bytes operatorData) public;
}

contract Spacedrop {
  using SafeERC20 for ERC20;

  mapping (bytes32 => bool) used;
  event Sent(address indexed token, address indexed sender, address indexed recipient, uint256 tokensToTransfer, uint256 nonce, uint256 iface);

  function validateAndRegisterClaim(address sender, bytes32 h, uint8 v, bytes32 r, bytes32 s) internal {
    // signer must be sender
    bytes memory prefix = "\x19Ethereum Signed Message:\n32";
    address signer = ecrecover(keccak256(prefix, h), v, r, s);
    require(signer == sender && signer != address(0));

    // check this claim hasn't been recorded already, then record it
    require(!used[h]);
    used[h] = true;
  }

  function claimTokensERC20(address token, address sender, address recipient, uint256 tokensToTransfer, uint256 nonce, uint8 v, bytes32 r, bytes32 s) public {
    bytes32 h = keccak256(token, sender, recipient, tokensToTransfer, nonce);
    validateAndRegisterClaim(sender, h, v, r, s);
    ERC20(token).safeTransferFrom(sender, recipient, tokensToTransfer);
    Sent(token, sender, recipient, tokensToTransfer, nonce, 20);
  }

  function claimTokensERC777(address token, address sender, address recipient, uint256 tokensToTransfer, uint256 nonce, bytes userData, uint8 v, bytes32 r, bytes32 s) public {
    bytes32 h = keccak256(token, sender, recipient, tokensToTransfer, nonce, userData);
    validateAndRegisterClaim(sender, h, v, r, s);
    ERC777i(token).operatorSend(sender, recipient, tokensToTransfer, userData, "spacedrop");
    Sent(token, sender, recipient, tokensToTransfer, nonce, 777);
  }
}
