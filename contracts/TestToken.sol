pragma solidity ^0.4.18;

import "../node_modules/zeppelin-solidity/contracts/token/ERC20/StandardToken.sol";

contract TestToken is StandardToken {
  function TestToken() public {
    totalSupply_ = 1000 * 1000 * 1000;
    balances[msg.sender] = totalSupply_;
  }
}
