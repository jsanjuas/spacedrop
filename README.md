Spacedrop
---------

Spacedrop implements an Ethereum airdrop scheme where recipients pay for
transaction gas costs, instead of the issuer.

Performing an airdrop the usual way can get costly, since it requires paying
for gas costs to send the tokens to a bunch of recipients.
With Ethereum mainnet transaction costs now
nearing $0.1, implementing a massive airdrop can get expensive. With Spacedrop, it is the recipients of the airdrop who pay for transaction costs, not you.

To set up a spacedrop, the issuer only needs to allow the Spacedrop contract to
distribute tokens on their behalf, for example using
`token.approve(spacedrop, total_tokens_to_distribute)` in the case of an
ERC20 token. Spacedrop is compatible with ERC20 and also ERC777 tokens.

Then, the airdrop issuer sends off-chain (e.g., via email) a signed message that
is unique for each recipient. Recipients can then send these messages to the
Spacedrop smart contract to retrieve their tokens via functions
`claimTokensERC20()` and `claimTokensERC777()`.

In particular, these messages are sort-of IOUs that include:
- The token to be transfered.
- The sender, recipient, and token amount.
- A nonce (more on why this nonce exists later).
- The signature of the sender.


With these parameters, the smart contract can verify that in fact the sender did
authorize this transaction, and execute them on their behalf.

Spacedrop checks that a user does not try to collect
his tokens twice (that is, calling `claimTokensERC20()` or `claimTokensERC777()` with the same parameters more
than once). This scheme would prevent the issuer from sending the same token
amount more than once.
Since a multi-phase airdrop could entail sending the same amount
of tokens to the same address multiple times, we allow the sender to provide
a different nonce each time, effectively working around this limitation.

The issuer of an aidrop can cancel the airdrop. In the case of an ERC20 token,
this can be achieved via
`token.approve(spacedrop, 0)`. Note that recipient's calls to `claimTokensERC20()`
will start failing, thus wasting their gas. The airdrop can be resumed by
increasing the token allowance again, and transactions that failed will
this time succeed.
Of course, an issuer can create a spacedrop that cannot be cancelled by sending
the tokens to a contract that cannot later stop token transfers.

Regarding costs. For the issuer, gas costs are reduced to setting
up the spacedrop via calling `approve()` as explained above; Spacedrop is
dramatically cheaper than a standard airdrop for the issuer. For recipients,
the cost is similar to a standard token transfer.

The aggregate cost for a Spacedrop, including gas paid by issuer and recipients,
is slighly higher than in a standard airdrop, since in the latter
several token transfers can be lumped together in a single transaction. That said,
the total cost should be in the same order of magnitude.

Note that in Spacedrop recipients must actively collect the tokens they are
granted. This may or may not be desirable. On one hand, the issuer can recover
unclaimed tokens. Spacedrop does not promote airdop spam, for better or worse.
Finally, recipients must of course own some ether in order to pay for transaction
costs. Again, this may not be desirable.

Interestingly, the Spacedrop contract has no owner and does not require
redeploying to implement different airdrops. None of that is required, since
all the necessary data is encoded in the IOUs that recipients relay to the
smart contract.

**TODO**: nice front-end to collect, backend code to automate a spacedrop.
So far, only the contract and a few tests are implemented.

**WARNING**: this code has not been extensively reviewed nor has it undergone
a formal code audit. One would expect few defects in a simple 32-line smart
contract, but a word of caution applies: use at your own risk.

References:
- [A post on the use of ec signatures](https://medium.com/@LibertyLocked/ec-signatures-and-recovery-in-ethereum-smart-contracts-560b6dd8876).
- [A post](https://blog.gridplus.io/a-simple-ethereum-payment-channel-implementation-2d320d1fad93) on payment channels [and another one](https://medium.com/@matthewdif/ethereum-payment-channel-in-50-lines-of-code-a94fad2704bc). Payment channels have similarities with the implementation of Spacedrop. Loosely speaking, Spacedrop and payment channels leverage off-chain IOUs that can be redeemed by sending them to a smart contract.
- [AirdropChannels](https://github.com/postables/Postables-Payment-Channel/blob/develop/solidity/AirDropChannels.sol) implement the same concept of a beneficiary-paid airdrop, although the solution is considerably more complex (212 lines of code vs 32 in Spacedrop).
